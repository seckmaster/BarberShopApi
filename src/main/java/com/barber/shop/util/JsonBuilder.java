package com.barber.shop.util;

import java.util.HashMap;

/**
 * Created by Toni Kocjan on 07/09/2017.
 * Triglav Partner BE
 */

public class JsonBuilder<T> {

  private HashMap<String, T> json = new HashMap<>();

  public JsonBuilder add(String key, T value) {
    json.put(key, value);
    return this;
  }

  public HashMap<String, T> json() {
    return json;
  }
}
