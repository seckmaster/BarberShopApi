package com.barber.shop.util;

import com.barber.shop.exception.MissingDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Toni Kocjan on 13/10/2017.
 * Triglav Partner BE
 */

public class Utils {

  private final static Logger logger = LoggerFactory.getLogger(Utils.class);

  public static boolean isNull(Object... objects) {
    for (Object o : objects) {
      if (o == null) return true;
    }
    return false;
  }

  public static boolean notNull(Object... objects) {
    for (Object o : objects) {
      if (o == null) return false;
    }
    return true;
  }

  public static void requireNotNull(Object... objects) throws MissingDataException {
    for (Object o : objects) {
      if (o == null) throw new MissingDataException();
    }
  }

  public static <T> List<T> asList(T... objects) {
    if (objects == null) return new ArrayList<>();
    return Arrays.asList(objects);
  }

  public static void closeStream(Closeable closeable) {
    if (closeable == null) {
      return;
    }

    try {
      closeable.close();
    } catch (Exception e) {
      logger.error("Error closing input stream: ", e);
    }
  }

  public static Optional<Long> convertToEpoch(String date) {
    try {
      final String pattern = "MMM dd, yyyy HH:mm:ss a";
      long time = new SimpleDateFormat(pattern).parse(date).getTime();
      return Optional.of(time);
    } catch (ParseException e) {
      logger.error("Error parsing date " + date + ": ", e);
      return Optional.empty();
    }
  }
}
