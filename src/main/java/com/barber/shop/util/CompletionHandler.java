package com.barber.shop.util;

/**
 * Created by Toni Kocjan on 06/11/2017.
 * Triglav Partner BE
 */

public interface CompletionHandler<T> {

  void completion(T response);
}
