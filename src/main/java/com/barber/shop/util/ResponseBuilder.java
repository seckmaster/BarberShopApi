package com.barber.shop.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;

/**
 * Created by Toni Kocjan on 16/08/2017.
 * Triglav Partner BE
 */

public class ResponseBuilder {

  private static final String ERROR_MESSAGE_LABEL = "errorMessage";

  private ResponseEntity.BodyBuilder bodyBuilder;
  private JsonBuilder jsonBuilder = new JsonBuilder();

  public ResponseBuilder(HttpStatus status) {
    this.bodyBuilder = ResponseEntity.status(status);
    add("code", status.value());
  }

  public ResponseBuilder(int statusCode) {
    this.bodyBuilder = ResponseEntity.status(statusCode);
    add("code", statusCode);
  }

  public ResponseBuilder add(String key, Object value) {
    jsonBuilder.add(key, value);
    return this;
  }

  public ResponseEntity build() {
    addTimestamp();
    return buildJson();
  }

  private void addTimestamp() {
    add("timestamp", new Date().getTime());
  }

  private ResponseEntity buildJson() {
    return bodyBuilder.body(jsonBuilder.json());
  }

  // Predefined responses

  public static ResponseBuilder OK() {
    return new ResponseBuilder(HttpStatus.OK);
  }

  public static ResponseBuilder UNAUTHORIZED() {
    return new ResponseBuilder(HttpStatus.UNAUTHORIZED).add(ERROR_MESSAGE_LABEL, "Authorization token is invalid or expired");
  }

  public static ResponseBuilder INVALID_INPUT() {
    return new ResponseBuilder(HttpStatus.BAD_REQUEST).add(ERROR_MESSAGE_LABEL, "Invalid input");
  }

  public static ResponseBuilder NOT_FOUND(String entity, Long id) {
    return new ResponseBuilder(HttpStatus.NOT_FOUND).add(ERROR_MESSAGE_LABEL, entity + " with id " + id + " not found");
  }

  public static ResponseBuilder NOT_FOUND(String message) {
    return new ResponseBuilder(HttpStatus.NOT_FOUND).add(ERROR_MESSAGE_LABEL, message);
  }

  public static ResponseBuilder INTERNAL_ERROR(String errMessage) {
    return new ResponseBuilder(HttpStatus.INTERNAL_SERVER_ERROR).add(ERROR_MESSAGE_LABEL, errMessage);
  }

  public static ResponseBuilder BAD_REQUEST(String errMessage) {
    return new ResponseBuilder(HttpStatus.BAD_REQUEST).add(ERROR_MESSAGE_LABEL, errMessage);
  }
}
