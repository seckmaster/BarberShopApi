package com.barber.shop.push.credentials;

import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Created by Toni Kocjan on 05/09/2017.
 * Triglav Partner BE
 */

@Component
public class StagingPushCredentialsFactory extends PushCredentialsFactory {

  private String protocol = "TLS";
  private String trustManagerName = "IBMJSSE2";

  @Override
  public SSLContext sslContext() throws NoSuchProviderException, NoSuchAlgorithmException {
    return SSLContext.getInstance(protocol, trustManagerName);
  }

  @Override
  public TrustManagerFactory trustManagerFactory(KeyStore keyStore) throws NoSuchAlgorithmException, KeyStoreException, NoSuchProviderException {
    TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm(), trustManagerName);
    factory.init(keyStore);
    return factory;
  }
}
