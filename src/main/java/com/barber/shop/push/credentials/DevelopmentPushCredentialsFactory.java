package com.barber.shop.push.credentials;

import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Toni Kocjan on 05/09/2017.
 * Triglav Partner BE
 */

@Component
public class DevelopmentPushCredentialsFactory extends PushCredentialsFactory {

  @Override
  public SSLContext sslContext() throws NoSuchAlgorithmException {
    return SSLContext.getInstance("TLS");
  }

  @Override
  public TrustManagerFactory trustManagerFactory(KeyStore keyStore) throws NoSuchAlgorithmException, KeyStoreException {
    TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    factory.init(keyStore);
    return factory;
  }
}
