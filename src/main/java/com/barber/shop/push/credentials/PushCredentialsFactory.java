package com.barber.shop.push.credentials;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

/**
 * Created by Toni Kocjan on 05/09/2017.
 * Triglav Partner BE
 */

public abstract class PushCredentialsFactory {

  public abstract SSLContext sslContext() throws NoSuchAlgorithmException, NoSuchProviderException;

  public abstract TrustManagerFactory trustManagerFactory(KeyStore keyStore) throws NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException;

  public KeyStore keyStore(InputStream cert, String password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
    KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(cert, password.toCharArray());
    return keyStore;
  }
}
