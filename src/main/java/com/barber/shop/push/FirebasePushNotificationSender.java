package com.barber.shop.push;

import com.barber.shop.push.credentials.PushCredentialsFactory;
import com.barber.shop.tools.Properties;
import com.barber.shop.tools.ResourceLoader;
import com.barber.shop.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Created by Toni Kocjan on 18/08/2017.
 * Triglav Partner BE
 */

@Component
public class FirebasePushNotificationSender implements PushNotificationService {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  private static final String CERT_PASSWORD = "vreme";

  @Autowired
  private PushCredentialsFactory pushCredentialsFactory;

  private String serverKey;
  private String url;

  @PostConstruct
  private void init() {
    serverKey = Properties.defaultProperties().getProperty("google.services.server_key");
    url = Properties.defaultProperties().getProperty("google.fcm.url");
  }

  @Override
//    @Async
  public boolean sendPushNotification(Serializable content) {
    InputStream cert = null;

    try {
      cert = loadCertificate();
      HttpsURLConnection connection = setupUrlConnection(cert, CERT_PASSWORD);
      setupOutputStream(connection, content);
      boolean result = handleResponse(connection);
      return result;
    } catch (Exception e) {
      return handleException(e);
    } finally {
      Utils.closeStream(cert);
    }
  }

  private InputStream loadCertificate() throws FileNotFoundException {
    return ResourceLoader.defaultResourceLoader().loadResource(ResourceLoader.Resource.KEY_STORE);
  }

  private HttpsURLConnection setupUrlConnection(InputStream certificate, String password) throws IOException, NoSuchProviderException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, CertificateException {
    URL url = new URL(this.url);

    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Authorization", "key=" + serverKey);
    connection.setDoOutput(true);

    SSLContext sslContext = pushCredentialsFactory.sslContext();
    KeyStore keyStore = pushCredentialsFactory.keyStore(certificate, password);
    TrustManagerFactory factory = pushCredentialsFactory.trustManagerFactory(keyStore);

    sslContext.init(null, factory.getTrustManagers(), new SecureRandom());
    connection.setSSLSocketFactory(sslContext.getSocketFactory());

    return connection;
  }

  private void setupOutputStream(HttpsURLConnection connection, Serializable content) throws IOException {
    DataOutputStream stream = new DataOutputStream(connection.getOutputStream());
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.writeValue((OutputStream) stream, content);
    stream.flush();
    stream.close();
  }

  private boolean handleException(Exception e) {
    logger.error("Sending Push notification failed with message: " + errorMessageForException(e));
    return false;
  }

  private String errorMessageForException(Exception e) {
    if (e instanceof FileNotFoundException) {
      return "Error loading certificate";
    } else {
      return e.getLocalizedMessage();
    }
  }

  private boolean handleResponse(HttpsURLConnection connection) throws IOException {
    int responseCode = connection.getResponseCode();
    if (responseCode == 200) {
      logger.debug("Push notification sent successfully");
      return true;
    } else {
      logger.debug("Sending Push notification failed");
      return false;
    }
  }
}
