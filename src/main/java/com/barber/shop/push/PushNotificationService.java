package com.barber.shop.push;

import java.io.Serializable;

/**
 * Created by Toni Kocjan on 04/09/2017.
 * Triglav Partner BE
 */

public interface PushNotificationService {
  boolean sendPushNotification(Serializable content);
}
