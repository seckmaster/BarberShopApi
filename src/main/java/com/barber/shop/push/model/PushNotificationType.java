package com.barber.shop.push.model;

/**
 * Created by Toni Kocjan on 07/09/2017.
 * Triglav Partner BE
 */

public enum PushNotificationType {
  ;

  PushNotificationType(String title, String message) {
    this.title = title;
    this.message = message;
  }

  private final String title;
  private final String message;

  public String getMessage() {
    return message;
  }

  public String getTitle() {
    return title;
  }
}
