package com.barber.shop.push.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Toni Kocjan on 11/09/2017.
 * Triglav Partner BE
 */

public class FirebaseDataContent implements Serializable {

  /**
   * Model:
   * {
   * "data": {
   * "key": "any"
   * },
   * "to": "token"
   * }
   */

  @JsonProperty
  private String to;
  @JsonProperty
  private Map<String, Object> data;

  public FirebaseDataContent(String title, String text, Map<String, Object> data, String to) {
    this.data = data;
    this.to = to;
    data.put("title", title);
    data.put("text", text);
  }

  public void put(String key, String value) {
    data.put(key, value);
  }

}
