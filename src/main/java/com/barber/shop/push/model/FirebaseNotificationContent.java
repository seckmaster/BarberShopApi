package com.barber.shop.push.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toni Kocjan on 18/08/2017.
 * Triglav Partner BE
 */

public class FirebaseNotificationContent implements Serializable {

  /**
   * Model:
   * {
   * "notification": {
   * "title": "Your Title",
   * "text": "Your Text"
   * },
   * "data": {
   * "key": "any"
   * },
   * "to": "token"
   * }
   */

  @JsonProperty
  private String to;
  @JsonProperty
  private Map<String, Object> data;
  @JsonProperty
  private Map<String, String> notification = new HashMap<>();

  public FirebaseNotificationContent(String title, String text, Map<String, Object> data, String to) {
    this.data = data;
    this.to = to;
    notification.put("title", title);
    notification.put("text", text);
  }

  public void put(String key, String value) {
    data.put(key, value);
  }

}
