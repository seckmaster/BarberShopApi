package com.barber.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class BarberShopApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(BarberShopApiApplication.class, args);
  }
}
