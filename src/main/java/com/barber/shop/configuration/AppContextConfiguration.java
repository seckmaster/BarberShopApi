package com.barber.shop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Created by Toni Kocjan on 20/09/2017.
 * Triglav Partner BE
 */

@Configuration
public class AppContextConfiguration extends WebMvcConfigurerAdapter {

  @Autowired
  private HandlerInterceptorAdapter authInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(authInterceptor);
  }
}
