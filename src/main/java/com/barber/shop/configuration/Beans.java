package com.barber.shop.configuration;

import com.barber.shop.provider.mail.LocalMailProvider;
import com.barber.shop.provider.mail.MailProvider;
import com.barber.shop.push.credentials.DevelopmentPushCredentialsFactory;
import com.barber.shop.push.credentials.PushCredentialsFactory;
import com.barber.shop.push.credentials.StagingPushCredentialsFactory;
import com.barber.shop.tools.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Created by Toni Kocjan on 05/09/2017.
 * Triglav Partner BE
 */

@Configuration
@PropertySource("classpath:application.properties")
public class Beans {

  @Bean
  public PushCredentialsFactory pushCredentialsFactory() {
    if (Properties.defaultProperties().onDevelopmentEnvironment()) return new DevelopmentPushCredentialsFactory();
    return new StagingPushCredentialsFactory();
  }

  @Bean
  public MailProvider mailProvider() {
    return new LocalMailProvider();
  }

  @Bean
  public JavaMailSender mailSender() {
    JavaMailSenderImpl sender = new JavaMailSenderImpl();
    sender.setHost("barber.shop@gmail.com");
    return sender;
  }
}
