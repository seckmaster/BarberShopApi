package com.barber.shop.configuration;

import com.barber.shop.tools.Properties;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toni Kocjan on 09/08/2017.
 * Triglav Partner BE
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories()
public class JpaConfiguration extends JpaBaseConfiguration {

  protected JpaConfiguration(@Lazy @Qualifier("dataSource") DataSource dataSource,
                             @Lazy @Qualifier("jpaProperties") JpaProperties properties,
                             ObjectProvider<JtaTransactionManager> jtaTransactionManagerProvider,
                             ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
    super(dataSource, properties, jtaTransactionManagerProvider, transactionManagerCustomizers);
  }

  @Bean
  public JdbcTemplate jdbcTemplate() {
    return new JdbcTemplate(dataSource());
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(final EntityManagerFactoryBuilder builder) {
    LocalContainerEntityManagerFactoryBean factoryBean = buildEntityManagerFactory(builder);
    return factoryBean;
  }

  private LocalContainerEntityManagerFactoryBean buildEntityManagerFactory(EntityManagerFactoryBuilder builder) {
    return builder.dataSource(dataSource())
        .packages("com.barber.shop.service")
        .properties(initJpaProperties())
        .build();
  }

  @Bean
  @Primary
  public DataSource dataSource() {
    return dataSourceForDevelopmentEnvironment();
  }

  private DataSource dataSourceForDevelopmentEnvironment() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
//    dataSource.setDriverClassName("");
    dataSource.setUrl("jdbc:mysql://localhost:3306/BarberShop");
    dataSource.setUsername("root");
    dataSource.setPassword("");
    return dataSource;
  }

  @Bean
  public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
    final JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf);
    return transactionManager;
  }

  @Bean JpaProperties jpaProperties() {
    return new JpaProperties();
  }

  @Override
  protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
    return new HibernateJpaVendorAdapter();
  }

  @Override
  protected Map<String, Object> getVendorProperties() {
    return initJpaProperties();
  }

  private final Map<String, Object> initJpaProperties() {
    final Map<String, Object> ret = new HashMap<>();
    return ret;
  }
}
