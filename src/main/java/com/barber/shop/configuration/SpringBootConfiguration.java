package com.barber.shop.configuration;

import com.barber.shop.BarberShopApiApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Toni Kocjan on 10/08/2017.
 * Triglav Partner BE
 */

@Configuration
public class SpringBootConfiguration extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(BarberShopApiApplication.class);
  }
}
