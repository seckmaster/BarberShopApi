package com.barber.shop.tools;

import com.barber.shop.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * Created by Toni Kocjan on 16/08/2017.
 * Triglav Partner BE
 */

public class Properties extends java.util.Properties {

  private final static Logger logger = LoggerFactory.getLogger(Properties.class);
  private static Properties properties;

  public static Properties defaultProperties() {
    if (properties == null) initializeProperties();
    return properties;
  }

  private static void initializeProperties() {
    properties = new Properties();
    loadProperties();
    loadEnvironmentProperties();
    loadLocalization();
  }

  private static void loadProperties() {
    InputStream is = null;
    try {
      is = ResourceLoader.defaultResourceLoader().loadResource(ResourceLoader.Resource.PROPERTIES);
      properties.load(is);
    } catch (Exception e) {
      logger.error("Error reading config.properties.", e);
    } finally {
      Utils.closeStream(is);
    }
  }

  private static void loadEnvironmentProperties() {
    loadLocalEnvironmentProperties();
  }

  private static void loadLocalEnvironmentProperties() {
    logger.warn("JVM variable app.klika.properties.basePath is undefined; using default properties.");
    loadResource(ResourceLoader.Resource.PROPERTIES);
  }

  private static void loadLocalization() {
//        loadResource(ResourceLoader.Resource.LOCALIZATION);
  }

  private static void loadResource(ResourceLoader.Resource resource) {
    InputStream is = null;
    try {
      is = ResourceLoader.defaultResourceLoader().loadResource(resource);
      properties.load(is);
    } catch (Exception e) {
      logger.error("Error reading config-env.properties.", e);
    } finally {
      Utils.closeStream(is);
    }
  }

  ///

  public boolean onDevelopmentEnvironment() {
    return properties.getProperty("environment", "local").equals("local");
  }
}
