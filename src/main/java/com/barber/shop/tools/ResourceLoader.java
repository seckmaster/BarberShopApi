package com.barber.shop.tools;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Created by Toni Kocjan on 16/08/2017.
 * Triglav Partner BE
 */

public class ResourceLoader {

  public enum Resource {
    PROPERTIES("config.properties"),
    KEY_STORE("");

    Resource(String resourceName) {
      this.resourceName = resourceName;
    }

    public String getResourceName() {
      return resourceName;
    }

    private String resourceName;
  }

  private static ResourceLoader resourceLoader;

  public static ResourceLoader defaultResourceLoader() {
    if (resourceLoader == null) resourceLoader = new ResourceLoader();
    return resourceLoader;
  }

  public InputStream loadResource(Resource resource) throws FileNotFoundException {
    return loadResource(resource.getResourceName());
  }

  public InputStream loadResource(String fileName) throws FileNotFoundException {
    Optional<InputStream> stream = resourceAsStream(fileName);
    if (!stream.isPresent()) throw new FileNotFoundException(fileName);
    return stream.get();
  }

  private Optional<InputStream> resourceAsStream(String fileName) {
    InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
    return Optional.ofNullable(inputStream);
  }
}
