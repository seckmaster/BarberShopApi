package com.barber.shop.tools.serialization;

import java.util.Base64;

/**
 * Created by Toni Kocjan on 17/07/2017.
 * Triglav Partner BE
 */

public class Base64Serializer {

  public static String deserialize(byte[] data) {
    if (data == null) return null;
    return Base64.getEncoder().encodeToString(data);
  }

  public static byte[] serialize(String base64) {
    if (base64 == null) return null;
    return Base64.getDecoder().decode(base64);
  }
}
