package com.barber.shop.service.interceptor;

import com.barber.shop.exception.NotAuthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

  public static final String SESSION_KEY_AUTH = "security.login";
  private static final Logger _log = LoggerFactory.getLogger(AuthInterceptor.class);

  private static final Set<String> unsecured = new HashSet<>();
  private static final Set<String> unreachableForLogged = new HashSet<>();
  private static final Set<String> ipLocked = new HashSet<>();
  // Parameters to ignore while rebuilding query string for language change and impersonation
  private static final Set<String> ignoredParameters = new HashSet<>();

  private static final String[] HEADERS_TO_TRY = {
      "X-Forwarded-For",
      "Proxy-Client-IP",
      "WL-Proxy-Client-IP",
      "HTTP_X_FORWARDED_FOR",
      "HTTP_X_FORWARDED",
      "HTTP_X_CLUSTER_CLIENT_IP",
      "HTTP_CLIENT_IP",
      "HTTP_FORWARDED_FOR",
      "HTTP_FORWARDED",
      "HTTP_VIA",
      "REMOTE_ADDR"
  };

  @Autowired
  private AuthenticationManager authenticationManager;

//	@Autowired
//    private UserCacheInterface userCache;

  static {
    unsecured.add("/order");
    unsecured.add("/order/updateStatus");
    unsecured.add("/error");
    unsecured.add("/user/login");
    unsecured.add("/user/reset-password");
    unsecured.add("/location/asistenca");
  }

  private final static String BEARER = "Bearer";

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    if (true || isOptionsRequest(request)) {
      return true;
    }

    //pass unsecured
    if (isUnsecured(requestPath(request))) {
      _log.trace("Usecured paths allowed for all");
      return true;
    }

    setHeadersForResponse(response);

    _log.trace("Entering AuthInterceptor.");

    Optional<String> auth = getAuthorization(request);
    if (!auth.isPresent()) {
      tokenNotPresent(response);
      return false;
    }

    return loginUser(request, auth.get());
  }

  private boolean isOptionsRequest(HttpServletRequest request) {
    return request.getMethod().equals(HttpMethod.OPTIONS.toString());
  }

  private String requestPath(HttpServletRequest request) {
    return request.getRequestURI().replaceAll(request.getContextPath(), "");
  }

  private boolean isUnsecured(String path) {
    return unsecured.contains(path) || path.contains("swagger");
  }

  private void setHeadersForResponse(HttpServletResponse response) {
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
    response.setHeader("x-content-type-options", "nosniff");
    response.setHeader("x-frame-options", "SAMEORIGIN");
    response.setHeader("x-xss-protection", "1; mode=block");
  }

  private Optional<String> getAuthorization(HttpServletRequest request) {
    if (request.getHeader("Authorization") == null) {
      return Optional.empty();
    }
    _log.trace(request.getHeader("Authorization"));
    return Optional.of(request.getHeader("Authorization"));
  }

  private void tokenNotPresent(HttpServletResponse response) {
    _log.info("Mobile API is disabled - denying access");

//        try {
//            Map<String, String> oauthResponse = new HashMap<>();
//            Gson gson = new Gson();
//
//            oauthResponse.put("status", "invalid_request");
//            oauthResponse.put("message", "No auth token was supplied");
//            response.setStatus(HttpStatus.BAD_REQUEST.value());
//            response.setHeader("WWW-Authenticate", "Bearer realm=\"itriglav\"");
//            response.setContentType("application/json");
//
//            PrintWriter writer = response.getWriter();
//            writer.println(gson.toJson(oauthResponse));
//            writer.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
  }

  private boolean loginUser(HttpServletRequest request, String authorizationToken) throws NotAuthorizedException {
    _log.trace("Token: " + authorizationToken);

//        UserEntity loggedUser = authenticationManager.isAuthorized(authorizationToken);
//        userCache.login(loggedUser, request);
    return true;
  }
}
