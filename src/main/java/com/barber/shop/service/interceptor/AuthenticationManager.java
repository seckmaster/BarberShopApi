package com.barber.shop.service.interceptor;

import com.barber.shop.exception.NotAuthorizedException;
import com.barber.shop.service.base.BaseManager;
import com.barber.shop.service.user.db.AuthEntity;
import com.barber.shop.service.user.db.UserEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

/**
 * Created by Toni Kocjan on 14/08/2017.
 * Triglav Partner BE
 */

@Component
@EnableTransactionManagement
public class AuthenticationManager extends BaseManager {

  @Transactional
  public UserEntity isAuthorized(String token) throws NotAuthorizedException {
    try {
      AuthEntity auth = (AuthEntity) em.createNamedQuery("AuthEntity.login")
          .setParameter("token", token)
          .getSingleResult();
      return auth.getUser();
    } catch (NoResultException e) {
      logger.debug("Invalid token", token);
      throw new NotAuthorizedException(token);
    }
  }

  @Transactional
  public AuthEntity createAuthToken(UserEntity user, String bearer) {
    AuthEntity auth = new AuthEntity();
    auth.setUser(user);
    auth.setToken(bearer);
    auth = em.merge(auth);
    em.flush();
    return auth;
  }
}
