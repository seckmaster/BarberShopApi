package com.barber.shop.service.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Toni Kocjan on 23/10/2017.
 * Triglav Partner BE
 */

public class ValidationError {

  @JsonProperty
  private List<String> errors = new ArrayList<>();
  @JsonProperty
  private final String errorMessage;
  @JsonProperty
  private final String code;
  @JsonProperty
  private final Long timestamp;

  public ValidationError(String errorMessage, HttpStatus status) {
    this.errorMessage = errorMessage;
    this.code = status.toString();
    this.timestamp = new Date().getTime();
  }

  public void addValidationError(String error) {
    errors.add(error);
  }

}
