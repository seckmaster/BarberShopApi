package com.barber.shop.service.error;

import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;

/**
 * Created by Toni Kocjan on 23/10/2017.
 * Triglav Partner BE
 */

public class ValidationErrorBuilder {

  public static ValidationError fromBindingErrors(Errors errors, HttpStatus status) {
    ValidationError error = new ValidationError("Validation failed. " + errors.getErrorCount() + " error(s)", status);
    errors.getAllErrors().stream()
        .forEach(objectError -> error.addValidationError(objectError.getDefaultMessage()));
    return error;
  }
}
