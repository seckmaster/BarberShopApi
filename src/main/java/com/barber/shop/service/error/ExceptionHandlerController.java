package com.barber.shop.service.error;

import com.barber.shop.exception.InputValidationException;
import com.barber.shop.exception.NotAuthorizedException;
import com.barber.shop.util.ResponseBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by Toni Kocjan on 23/10/2017.
 * Triglav Partner BE
 */

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    logger.debug("MethodArgumentNotValidException ", ex);
    ValidationError error = ValidationErrorBuilder.fromBindingErrors(ex.getBindingResult(), status);
    return super.handleExceptionInternal(ex, error, headers, status, request);
  }

  @ExceptionHandler(InputValidationException.class)
  public ResponseEntity handleValidationFailedException(InputValidationException e) {
    logger.debug("InputValidationException ", e);
    return ResponseBuilder.BAD_REQUEST(e.getMessage()).build();
  }

  @ExceptionHandler(NotAuthorizedException.class)
  public ResponseEntity handleNotAuthorizedException(NotAuthorizedException e) {
    logger.debug("NotAuthorizedException ", e);
    return ResponseBuilder.UNAUTHORIZED().build();
  }

}
