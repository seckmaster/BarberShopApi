package com.barber.shop.service.booking.business;

import com.barber.shop.service.booking.model.BookingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

@Component
public class BookingInteractorImpl implements BookingInteractor {

  @Autowired
  private BookingWorker bookingWorker;

  @Override
  public BookingResponse booking(Date from, Date to) {
    return bookingWorker.booking(from, to);
  }
}
