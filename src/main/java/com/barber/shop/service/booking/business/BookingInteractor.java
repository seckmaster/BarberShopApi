package com.barber.shop.service.booking.business;

import com.barber.shop.service.booking.model.BookingResponse;

import java.util.Date;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

public interface BookingInteractor {
  BookingResponse booking(Date from, Date to);
}
