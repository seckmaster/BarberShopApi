package com.barber.shop.service.booking.db;

import com.barber.shop.service.base.BaseManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

@Component
@EnableTransactionManagement
public class BookingManager extends BaseManager {

  @Transactional
  public List<BookingEntity> booking(@NotNull Date from, @NotNull Date to) {
    return em.createNamedQuery("BookingEntity.bookingsFromTo")
        .setParameter("fromDate", from)
        .setParameter("endDate", to)
        .getResultList();
  }

  @Transactional
  public List<WorkdayEntity> workday(@NotNull Date from, @NotNull Date to) {
    return em.createNamedQuery("WorkdayEntity.workdayFromTo")
        .setParameter("fromDate", from)
        .setParameter("toDate", to)
        .getResultList();
  }
}
