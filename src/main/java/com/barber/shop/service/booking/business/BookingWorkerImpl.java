package com.barber.shop.service.booking.business;

import com.barber.shop.service.booking.db.BookingEntity;
import com.barber.shop.service.booking.db.BookingManager;
import com.barber.shop.service.booking.db.WorkdayEntity;
import com.barber.shop.service.booking.model.BookingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

@Component
public class BookingWorkerImpl implements BookingWorker {

  @Autowired
  private BookingManager bookingManager;

  @Override
  public BookingResponse booking(@NotNull Date from, @NotNull Date to) {
    List<BookingEntity> bookings = bookingManager.booking(from, to);
    List<WorkdayEntity> workdays = bookingManager.workday(from, to);

    List<BookingResponse.Booking> bookingsResponse = new ArrayList<>();

    for (WorkdayEntity workdayEntity: workdays) {
      Date start = workdayEntity.getBeginDate();

      for (BookingEntity bookingEntity : bookings) {
        if (start.equals(bookingEntity.getBeginDate())) {
          start = bookingEntity.getEndDate();
        }
        else {
          Date end = bookingEntity.getBeginDate();
          bookingsResponse.add(new BookingResponse.Booking(start, end));
          start = bookingEntity.getBeginDate();
        }
      }
    }

    return new BookingResponse(bookingsResponse);
  }
}
