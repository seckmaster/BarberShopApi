package com.barber.shop.service.booking.model;

import com.barber.shop.service.base.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

public class BookingResponse extends BaseResponse {

  public static class Booking {
    Date from;
    Date to;

    public Booking(Date from, Date to) {
      this.from = from;
      this.to = to;
    }
  }

  public class Workday {
    Long workerId;
    List<Booking> workday;

  }

  @JsonProperty
  private List<Booking> booking;

  public BookingResponse(List<Booking> booking) {
    this.booking = booking;
  }
}
