package com.barber.shop.service.booking.db;

import com.barber.shop.service.base.BaseEntity;
import com.barber.shop.service.user.db.UserEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

@Entity
@Table(name = "Workday")
@NamedQueries({
    @NamedQuery(name = "WorkdayEntity.workdayFromTo", query = "SELECT workday from WorkdayEntity workday WHERE workday.beginDate >= :fromDate AND workday.endDate <= :toDate")
})
public class WorkdayEntity extends BaseEntity {

  @Basic
  @Column(name = "BEGIN_DATE")
  private Date beginDate;

  @Basic
  @Column(name = "END_DATE")
  private Date endDate;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "BARBER_ID")
  private UserEntity barber;

  public Date getBeginDate() {
    return beginDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public UserEntity getBarber() {
    return barber;
  }

  public void setBarber(UserEntity barber) {
    this.barber = barber;
  }

  public void setBeginDate(Date beginDate) {
    this.beginDate = beginDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
}
