package com.barber.shop.service.booking;

import com.barber.shop.service.base.BaseResource;
import com.barber.shop.service.booking.business.BookingInteractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Toni Kocjan on 12/03/2018.
 * BarberShopAPI
 */

@Component
@RequestMapping(value = "/booking")
public class BookingResource extends BaseResource {

  @Autowired
  private BookingInteractor bookingInteractor;

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity booking() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(2018, 3, 12, 8, 0, 0);
    Date from = calendar.getTime();
    calendar.set(2018, 3, 16, 0, 0, 0);
    Date to = calendar.getTime();

    return ResponseEntity.ok(bookingInteractor.booking(from, to));
  }
}

/*

V soboto, 17. marca ob 13. uri bo v Novem mestu v športni dvorani Marof potekalo 5. kolo slovenske boksarske lige, ki ga organizira BK Boxeo.
Vljudno vabljeni vsi ljubitelji borilnih veščin.

 */