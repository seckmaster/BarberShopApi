package com.barber.shop.service.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Toni Kocjan on 14/08/2017.
 * Triglav Partner BE
 */

public abstract class BaseManager {

  protected static final Logger logger = LoggerFactory.getLogger(BaseManager.class);

  @PersistenceContext
  protected EntityManager em;
}
