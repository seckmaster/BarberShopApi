package com.barber.shop.service.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Toni Kocjan on 14/08/2017.
 * Triglav Partner BE
 */

public class BaseResource {

  protected final Logger logger = LoggerFactory.getLogger(getClass());
}
