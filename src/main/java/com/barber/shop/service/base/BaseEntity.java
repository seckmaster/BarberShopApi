package com.barber.shop.service.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Toni Kocjan on 09/08/2017.
 * Triglav Partner BE
 */

@MappedSuperclass
public class BaseEntity implements Serializable {

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Long id;

  @Version
  @Column(name = "LAST_CHANGED")
  @JsonIgnore
  protected Timestamp lastChanged;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Timestamp getLastChanged() {
    return lastChanged;
  }

  public void setLastChanged(Timestamp last_changed) {
    this.lastChanged = last_changed;
  }
}
