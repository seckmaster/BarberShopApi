package com.barber.shop.service.user.db;

import com.barber.shop.service.base.BaseEntity;

import javax.persistence.*;

/**
 * Created by Toni Kocjan on 11/08/2017.
 * Triglav Partner BE
 */

@Entity
@Table(name = "AUTH", schema = "ZTPA")
@NamedQueries({
    @NamedQuery(name = "AuthEntity.login", query = "SELECT a from AuthEntity a WHERE a.token = :token")
})
public class AuthEntity extends BaseEntity {

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USER")
  private UserEntity user;

  @Basic
  @Column(name = "TOKEN")
  private String token;

  public UserEntity getUser() {
    return user;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }
}
