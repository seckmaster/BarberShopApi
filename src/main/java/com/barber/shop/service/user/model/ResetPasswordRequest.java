package com.barber.shop.service.user.model;

import com.barber.shop.service.base.BaseRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Toni Kocjan on 06/09/2017.
 * Triglav Partner BE
 */

public class ResetPasswordRequest extends BaseRequest {

  @JsonProperty
  String email;
  @JsonProperty
  String mobilePhoneNumber;

  public boolean isValid() {
    return email != null && mobilePhoneNumber != null;
  }

  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

  public String getEmail() {
    return email;
  }
}
