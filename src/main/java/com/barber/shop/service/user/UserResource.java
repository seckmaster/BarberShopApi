package com.barber.shop.service.user;

import com.barber.shop.service.base.BaseResource;
import com.barber.shop.service.user.model.LoginUserRequest;
import com.barber.shop.service.user.model.LoginUserResponse;
import com.barber.shop.service.user.model.ProfileResponse;
import com.barber.shop.service.user.model.ResetPasswordRequest;
import com.barber.shop.util.ResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Toni Kocjan on 10/08/2017.
 * Triglav Partner BE
 */

@Component
@RequestMapping(value = "/user")
@Api(value = "/user", description = "User management base")
public class UserResource extends BaseResource {

  @Autowired
  private UserWorkerInterface userWorker;

  @RequestMapping(value = "/profile", method = GET)
  @ApiOperation(value = "Get profile", response = ProfileResponse.class)
  public ResponseEntity getProfile(HttpServletRequest request) {
    ProfileResponse response = userWorker.profile(request);
    return ResponseEntity.ok(response);
  }

  @RequestMapping(value = "/login", method = POST)
  @ApiOperation(value = "Login user", response = LoginUserResponse.class)
  public ResponseEntity login(@RequestBody @Valid LoginUserRequest input) {
    try {
      LoginUserResponse response = userWorker.login(input);
      return ResponseEntity.ok(response);
    } catch (NoResultException e) {
      return ResponseBuilder.BAD_REQUEST("User not found").build();
    }
  }

  @RequestMapping(value = "/logout", method = GET)
  public ResponseEntity logout(HttpServletRequest request) {
    userWorker.logout(request);
    return ResponseEntity.ok().build();
  }

  @RequestMapping(value = "/reset-password", method = POST)
  @ApiOperation(value = "Reset password")
  public ResponseEntity resetPassword(@RequestBody @Valid ResetPasswordRequest input) {
    userWorker.resetPassword(input);
    return ResponseEntity.ok().build();
  }
}
