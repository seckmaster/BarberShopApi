package com.barber.shop.service.user;

import com.barber.shop.service.base.BaseWorker;
import com.barber.shop.service.user.db.UserManager;
import com.barber.shop.service.user.model.LoginUserRequest;
import com.barber.shop.service.user.model.LoginUserResponse;
import com.barber.shop.service.user.model.ProfileResponse;
import com.barber.shop.service.user.model.ResetPasswordRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Toni Kocjan on 15/09/2017.
 * Triglav Partner BE
 */

@Component
public class UserWorker extends BaseWorker implements UserWorkerInterface {

  @Autowired
  private UserManager userManager;

  @Override
  public ProfileResponse profile(HttpServletRequest request) {
    return new ProfileResponse(userManager.users().get(0));
  }

  @Override
  public void resetPassword(ResetPasswordRequest request) {

  }

  @Override
  public LoginUserResponse login(LoginUserRequest input) {
    return null;
  }

  @Override
  public void logout(HttpServletRequest request) {

  }
}
