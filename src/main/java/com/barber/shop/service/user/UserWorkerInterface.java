package com.barber.shop.service.user;

import com.barber.shop.service.user.model.LoginUserRequest;
import com.barber.shop.service.user.model.LoginUserResponse;
import com.barber.shop.service.user.model.ProfileResponse;
import com.barber.shop.service.user.model.ResetPasswordRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Toni Kocjan on 15/09/2017.
 * Triglav Partner BE
 */

public interface UserWorkerInterface {
  ProfileResponse profile(HttpServletRequest request);

  void resetPassword(ResetPasswordRequest request);

  LoginUserResponse login(LoginUserRequest input);

  void logout(HttpServletRequest request);
}
