package com.barber.shop.service.user.db;

import com.barber.shop.service.base.BaseManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by Toni Kocjan on 14/08/2017.
 * Triglav Partner BE
 */

@Component
@EnableTransactionManagement
public class UserManager extends BaseManager {

  @Transactional
  public List<UserEntity> users() {
    return em.createNamedQuery("UserEntity.allUsers").getResultList();
  }
}
