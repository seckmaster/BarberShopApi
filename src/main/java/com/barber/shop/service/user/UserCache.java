package com.barber.shop.service.user;

import com.barber.shop.service.interceptor.AuthInterceptor;
import com.barber.shop.service.user.db.UserEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Toni Kocjan on 19/09/2017.
 * Triglav Partner BE
 */

@Component
public class UserCache implements UserCacheInterface {

  public UserEntity getUser(HttpServletRequest request) {
    return (UserEntity) request.getSession().getAttribute(AuthInterceptor.SESSION_KEY_AUTH);
  }

  public void login(UserEntity user, HttpServletRequest request) {
    request.getSession().setAttribute(AuthInterceptor.SESSION_KEY_AUTH, user);
  }

  public void logout(HttpServletRequest request) {
    request.getSession().removeAttribute(AuthInterceptor.SESSION_KEY_AUTH);
  }
}
