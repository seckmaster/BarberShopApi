package com.barber.shop.service.user.model;

import com.barber.shop.service.base.BaseResponse;
import com.barber.shop.service.user.db.AuthEntity;

/**
 * Created by Toni Kocjan on 11/08/2017.
 * Triglav Partner BE
 */

public class LoginUserResponse extends BaseResponse {

  private String authorization;

  public LoginUserResponse(AuthEntity auth) {
    this.authorization = auth.getToken();
  }

  public String getAuthorization() {
    return authorization;
  }

  public void setAuthorization(String authorization) {
    this.authorization = authorization;
  }
}
