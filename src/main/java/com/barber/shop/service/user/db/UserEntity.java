package com.barber.shop.service.user.db;

import com.barber.shop.service.base.BaseEntity;

import javax.persistence.*;

/**
 * Created by Toni Kocjan on 09/08/2017.
 * Triglav Partner BE
 */

@Entity
@Table(name = "USER", schema = "BarberShop")
@NamedQueries({
  @NamedQuery(name = "UserEntity.allUsers", query = "SELECT u FROM UserEntity u")
})
public class UserEntity extends BaseEntity {

  @Basic
  @Column
  private String name;

  @Basic
  @Column
  private String lastname;

  @Basic
  @Column
  private String email;
}
