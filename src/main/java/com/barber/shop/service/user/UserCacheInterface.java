package com.barber.shop.service.user;

import com.barber.shop.service.user.db.UserEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Toni Kocjan on 23/10/2017.
 * Triglav Partner BE
 */

public interface UserCacheInterface {
  UserEntity getUser(HttpServletRequest request);

  void login(UserEntity user, HttpServletRequest request);

  void logout(HttpServletRequest request);
}
