package com.barber.shop.service.user.model;

import com.barber.shop.service.base.BaseRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by Toni Kocjan on 11/08/2017.
 * Triglav Partner BE
 */

public class LoginUserRequest extends BaseRequest {

  @NotBlank(message = "Email must not be blank")
  @JsonProperty
  private String email;

  @NotBlank(message = "Password must not be blank")
  @JsonProperty
  private String password;

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }
}
