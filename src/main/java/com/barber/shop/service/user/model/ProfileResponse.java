package com.barber.shop.service.user.model;

import com.barber.shop.service.base.BaseResponse;
import com.barber.shop.service.user.db.UserEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Toni Kocjan on 01/09/2017.
 * Triglav Partner BE
 */

public class ProfileResponse extends BaseResponse {

  @JsonProperty
  private UserEntity profile;

  public ProfileResponse(UserEntity user) {
    this.profile = user;
  }

}
