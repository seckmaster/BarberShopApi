package com.barber.shop.provider.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * Created by Toni Kocjan on 04/08/2017.
 * Triglav Partner BE
 */

@Component
public final class RestProvider implements RestProviderInterface {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private RestTemplate restTemplate;

  @PostConstruct
  private void init() {
//        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
//        httpRequestFactory.setConnectionRequestTimeout(30);
//        httpRequestFactory.setConnectTimeout(30);
//        httpRequestFactory.setReadTimeout(30);
    restTemplate = new RestTemplate();
  }

  public <T> ResponseEntity<T> consume(String url, HttpMethod method, HttpEntity<String> headers, Class targetClass) {
    try {
      logger.info("[" + method.toString() + " ] Request to '" + url + "' did begin ...");
      ResponseEntity<T> response = restTemplate.exchange(url, method, headers, targetClass);
      logger.info("Request to '" + url + "' did complete with status " + response.getStatusCodeValue());
      return response;
    } catch (HttpClientErrorException e) {
      logger.error("Request to " + url + " did complete with error: ", e);
      return new ResponseEntity(e.getStatusCode());
    } catch (Exception e) {
      logger.error("Request to " + url + " did complete with error: ", e);
      return ResponseEntity.badRequest().build();
    }
  }

  public <T> ResponseEntity<T> post(String url, Object request, Class targetClass) {
    try {
      logger.info("[POST] Request to '" + url + "' did begin ...");
      ResponseEntity<T> response = restTemplate.postForEntity(url, request, targetClass);
      logger.info("Request to '" + url + "' did complete with status " + response.getStatusCodeValue());
      return response;
    } catch (HttpClientErrorException e) {
      logger.error("Request to " + url + " did complete with error: ", e);
      return new ResponseEntity(e.getStatusCode());
    } catch (Exception e) {
      logger.error("Request to " + url + " did complete with error: ", e);
      return ResponseEntity.badRequest().build();
    }
  }
}
