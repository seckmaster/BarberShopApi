package com.barber.shop.provider.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * Created by Toni Kocjan on 26/10/2017.
 * Triglav Partner BE
 */

public interface RestProviderInterface {
  <T> ResponseEntity<T> consume(String url, HttpMethod method, HttpEntity<String> headers, Class targetClass);

  <T> ResponseEntity<T> post(String url, Object request, Class targetClass);
}
