package com.barber.shop.provider.mail;

/**
 * Created by Toni Kocjan on 12/09/2017.
 * Triglav Partner BE
 */

public enum MailType {
  RESET_PASSWORD_BOSS("Ponastavitev gesla - šef"),
  RESET_PASSWORD_WORKER("Ponastavitev gesla - delavec"),
  CREATE_WORKER("Ustvarjen je nov račun");

  MailType(String subject) {
    this.subject = subject;
  }

  public String getSubject() {
    return subject;
  }

  private String subject;
}
