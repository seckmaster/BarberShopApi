package com.barber.shop.provider.mail;

import javax.activation.DataSource;
import java.util.List;

/**
 * Created by Toni Kocjan on 11/09/2017.
 * Triglav Partner BE
 */

public interface MailProvider {
  void sendEmail(String to, String subject, String message);

  void sendEmail(String to, String replyTo, String subject, String message);

  void sendEmail(List<String> to, String subject, String message);

  void sendEmail(List<String> to, String replyTo, String subject, String message);

  void sendEmail(List<String> to, String replyTo, String subject, String message, DataSource dataSource);

  void sendEmail(MailPreparator preparator);
}
