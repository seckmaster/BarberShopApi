package com.barber.shop.provider.mail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toni Kocjan on 12/09/2017.
 * Triglav Partner BE
 */

public class MailPreparator {

  private List<String> destinationAddresses = new ArrayList<>();
  private String subject;
  private String message;
  private String ccAddress;
  private String replyToAddress;

  public MailPreparator addDestinationAddress(String to) {
    this.destinationAddresses.add(to);
    return this;
  }

  public MailPreparator setMessage(String message) {
    this.message = message;
    return this;
  }

  public MailPreparator setReplyToAddress(String replyToAddress) {
    this.replyToAddress = replyToAddress;
    return this;
  }

  public MailPreparator setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public MailPreparator setCcAddress(String ccAddress) {
    this.ccAddress = ccAddress;
    return this;
  }

  public String getSubject() {
    return subject;
  }

  public String getMessage() {
    return message;
  }

  public List<String> getDestinationAddresses() {
    return destinationAddresses;
  }

  public String getCcAddress() {
    return ccAddress;
  }

  public String getReplyToAddress() {
    return replyToAddress;
  }
}
