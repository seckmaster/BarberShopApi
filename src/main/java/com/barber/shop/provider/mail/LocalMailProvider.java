package com.barber.shop.provider.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toni Kocjan on 12/09/2017.
 * Triglav Partner BE
 */

@Component
public class LocalMailProvider implements MailProvider {

  @Autowired
  private JavaMailSender mailSender;

  @Override
  public void sendEmail(String to, String subject, String message) {
    sendEmail(to, null, subject, message);
  }

  @Override
  public void sendEmail(String to, String replyTo, String subject, String message) {
    List<String> addresses = new ArrayList<>(1);
    addresses.add(to);
    sendEmail(addresses, replyTo, subject, message);
  }

  @Override
  public void sendEmail(List<String> to, String subject, String message) {
    sendEmail(to, null, subject, message);
  }

  @Override
  public void sendEmail(List<String> to, String replyTo, String subject, String message) {
    sendEmail(to, replyTo, subject, message, null);
  }

  @Override
  public void sendEmail(List<String> to, String replyTo, String subject, String message, DataSource dataSource) {
    try {
      MimeMessagePreparator preparator = mimeMessage -> {
        List<InternetAddress> emailAddresses = addresses(to);
        mimeMessage.setRecipients(Message.RecipientType.TO, emailAddresses.toArray(new InternetAddress[emailAddresses.size()]));
        mimeMessage.setFrom(new InternetAddress("confirmOrder.si.sportradar.ag"));
        mimeMessage.setSubject(subject);
        mimeMessage.setText(message);
      };

      this.mailSender.send(preparator);
    } catch (MailException ex) {
      System.err.println(ex.getMessage());
    }
  }

  private List<InternetAddress> addresses(List<String> to) throws AddressException {
    List<InternetAddress> emailAddresses = new ArrayList<>();
    for (String address : to) {
      emailAddresses.add(new InternetAddress(address));
    }
    return emailAddresses;
  }

  @Override
  public void sendEmail(MailPreparator preparator) {
    sendEmail(
        preparator.getDestinationAddresses(),
        preparator.getReplyToAddress(),
        preparator.getSubject(),
        preparator.getMessage());
  }
}
