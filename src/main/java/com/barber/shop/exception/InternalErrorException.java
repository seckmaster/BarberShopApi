package com.barber.shop.exception;

/**
 * Created by Toni Kocjan on 16/11/2017.
 * Triglav Partner BE
 */

public class InternalErrorException extends Exception {

  public InternalErrorException() {
  }

  public InternalErrorException(String message) {
    super(message);
  }
}
