package com.barber.shop.exception;

/**
 * Created by Toni Kocjan on 17/08/2017.
 * Triglav Partner BE
 */

public class MissingDataException extends Exception {

  public MissingDataException() {
  }

  public MissingDataException(String message) {
    super(message);
  }

}
