package com.barber.shop.exception;

/**
 * Created by Toni Kocjan on 14/08/2017.
 * Triglav Partner BE
 */

public class NotAuthorizedException extends Exception {
  private String token;

  public NotAuthorizedException(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }
}
