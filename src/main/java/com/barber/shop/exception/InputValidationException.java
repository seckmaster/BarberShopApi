package com.barber.shop.exception;

/**
 * Created by Toni Kocjan on 11/09/2017.
 * Triglav Partner BE
 */

public class InputValidationException extends RuntimeException {

  public InputValidationException(String message) {
    super(message);
  }

}
