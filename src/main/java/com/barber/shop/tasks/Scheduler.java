package com.barber.shop.tasks;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * Created by Toni Kocjan on 16/08/2017.
 * Triglav Partner BE
 */

@Component
@EnableScheduling
public class Scheduler {

}
