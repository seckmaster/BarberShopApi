package com.barber.shop.tasks;

/**
 * Created by Toni Kocjan on 17/11/2017.
 * Triglav Partner BE
 */

public interface Task {

  void perform();
}
